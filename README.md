# Pharmacodynamic Model of Tumor Growth
An example of how to use modelbase v.1.2.3.

Replication of the pharmacodynamic model of Tumor Growth after administration of anticancer agents published by Simeoni et al, CANCER RESEARCH 64, 1094 –1101, February 1, 2004
